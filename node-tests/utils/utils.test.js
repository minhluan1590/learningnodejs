const utils = require('./utils');
const expect = require('expect');

it('should add two numbers', () => {
    var res = utils.add(33, 11);
    expect(res).toBe(44);
});

it('should square a number', () => {
    var res = utils.square(3);
    expect(res).toBe(9);
});

it('should set firstName and lastName', () => {
    var user = {location: 'Philadelphia', age: 25};
    var res = utils.setName(user, 'Andrew Mead');

    expect(user).toEqual(res);
});

it('should async add two numbers', (done) => {
    utils.asyncAdd(4, 3, (sum) => {
        expect(sum).toBe(7);
        done();
    });
});

it('should async square a number', (done) => {
    utils.asyncSquare(5, (res) => {
        expect(res).toBe(25);
        done();
    });
});